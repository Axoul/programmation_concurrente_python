import sys
out = 0
for arg in sys.argv[1:]:
    try:
        out = out + float(arg)
    except ValueError:
        print("Veuillez entrer des nombres en arguments")
        exit()
    if float(arg) < 0 or float(arg) > 20.0:
        print("Les notes doivent être comprisent entre 0 et 20")
        exit()

out = out/(len(sys.argv)-1)
print ("Moyenne = %.2f" % out)
