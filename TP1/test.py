import os
import sys

n = 0
pid = os.fork()
if pid != 0:
    n += 1
    p, s = os.wait()
    if os.WIFEXITED(s):
        n = n + os.WEXITSTATUS(s)

else:
    n += 10

print(os.getpid(), n)
sys.exit(n)
