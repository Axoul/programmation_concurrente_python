import os
import sys

pid = os.fork()
if (pid == 0):
    os.execlp("ls", "ls")
    print(1)
    sys.exit(0)
else:
    print(3)
