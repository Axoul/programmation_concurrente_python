import os
import sys

if(os.fork()==0):
    if(os.fork()==0):
        os.execlp("who","who")
    else:
        os.execlp("ps","ps")
else:
    os.execlp("ls","ls","-l")


if(os.fork()==0):
    if(os.fork()==0):
        os.execlp("who","who")
    else:
        os.wait()
        os.execlp("ps","ps")
else:
    os.wait()
    os.execlp("ls","ls","-l")
