import os
n = 0
for i in range(1, 5):
    fils_pid = os.fork()
    if (fils_pid > 0):  # Pour le pere du fils et du petit fils, fils_pid vaut 0
        os.wait()  # Grace au wait le programme attends la création des fork pour continuer donc il est déterministe dans ce cas
        n = i * 2
        break
print("n = ", n)
os._exit(0)
