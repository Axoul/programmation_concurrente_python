import os
import sys
import time

try:
    N = int(sys.argv[1])
except ValueError:
    print("Veuillez entrer un nombre en argument")
    exit()

for i in range(N):
    pid = os.fork()
    if (pid == 0):
        print("%d je suis le processus : %d, mon pere est : %d" % (i, os.getpid(), os.getppid()))
        time.sleep(2*i)
        os._exit(i)
    else:
        pid_fils, etat = os.wait()
        print("%d killé, etat %d" % (pid_fils,etat>>8))
