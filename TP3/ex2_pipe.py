import os, sys
import multiprocessing as mp

(dfr,dfw) = mp.Pipe()
pid = os.fork()

if pid:
    print("le processus %d : cat" % (os.getpid()))
    dfr.close()
    os.dup2(dfw.fileno(), sys.stdout.fileno())
    dfw.close()
    os.execlp("cat", "cat", "monFichier")

else:
    print("le processus %d : wc -l" % (os.getpid()))
    dfw.close()
    os.dup2(dfr.fileno(), sys.stdin.fileno())
    dfr.close()
    os.execlp("wc", "wc", "-l")

sys.exit(0)
