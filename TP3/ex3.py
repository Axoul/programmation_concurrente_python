import signal, sys, time
aff = False
def sig(s, frame):
    global aff
    aff = not aff

signal.signal(signal.SIGINT, sig)
while True:
    time.sleep(1)
    print(aff)
