# Groupe C
# Mallol Fabien
# Chabot Axel

import sys
import multiprocessing as mp

L = [15, 45, 89, 65, 89, 4, 5, 67, 80, 12, 56, 38, 92, 74, 80]
N = len(L) - 1

def process1(m, s):
    global L, N
    i = 1
    SommeImpairs = 0
    while(i <= N):
        SommeImpairs += L[i]
        i = i + 2

    m.acquire()
    s.value += SommeImpairs
    m.release()

def process2(m, s):
    global L, N
    i = 0
    SommePairs = 0
    while(i <= N):
        SommePairs += L[i]
        i = i + 2

    m.acquire()
    s.value += SommePairs
    m.release()

mutex = mp.Lock()
somme = mp.Value('i', 0)
p1 = mp.Process(target=process1, args=(mutex, somme))
p2 = mp.Process(target=process2, args=(mutex, somme))
p1.start()
p2.start()
p1.join()
p2.join()
print(somme.value)
sys.exit(0)
