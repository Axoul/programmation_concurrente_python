# Groupe C
# Mallol Fabien
# Chabot Axel

import sys
import multiprocessing as mp

def processA(s1):
    print("A")
    s1.release()
    s1.release()
    s1.release()

def processB(s1, s2):
    s1.acquire()
    print("B")
    s2.release()

def processC(s1, s2):
    s1.acquire()
    print("C")
    s2.release()

def processD(s1,s3):
    s1.acquire()
    print("D")
    s3.release()

def processE(s2,s3):
    s2.acquire()
    s2.acquire()
    print("E")
    s3.release()

def processF(s3):
    s3.acquire()
    s3.acquire()
    print("F")

sem1 = mp.Semaphore(0)
sem2 = mp.Semaphore(0)
sem3 = mp.Semaphore(0)
processes = []
processes.append(mp.Process(target=processA, args=(sem1,)))
processes.append(mp.Process(target=processB, args=(sem1, sem2)))
processes.append(mp.Process(target=processC, args=(sem1, sem2)))
processes.append(mp.Process(target=processD, args=(sem1, sem3)))
processes.append(mp.Process(target=processE, args=(sem2, sem3)))
processes.append(mp.Process(target=processF, args=(sem3,)))
for p in processes:
    p.start()

for p in processes:
    p.join()

sys.exit(0)
