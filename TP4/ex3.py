# Groupe C
# Mallol Fabien
# Chabot Axel

import sys, time
import multiprocessing as mp

def process1(s):
    print("1-Alors il fait ca")
    print("1-Puis ca")
    time.sleep(2)
    print("1-Et encore ca")
    s.release()
    time.sleep(2)
    print("Tache 1 finie")

def process2(s):
    print("2-Alors il fait ca")
    print("2-Puis ca")
    print("2-Et encore ca")
    s.acquire()
    print("Tache 2 finie")

sem = mp.Semaphore(0)
p1 = mp.Process(target=process1, args=(sem,))
p2 = mp.Process(target=process2, args=(sem,))

p1.start()
p2.start()
p1.join()
p2.join()
sys.exit(0)
