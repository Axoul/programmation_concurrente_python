# Groupe C
# Mallol Fabien
# Chabot Axel

import multiprocessing as mp
import time
import random

def P1(queue):
    while True:
        n = random.randint(1, 100)
        print("P1 envoie: ", n)
        queue.put(n)
        time.sleep(2)


def P2(queue):
    while True:
        n = random.randint(1, 100)
        print("P2 envoie: ", n)
        queue.put(n)
        time.sleep(1)


def C1(sema1, sema2, queue):
    while True:
        sem2.acquire()
        n1 = queue.get()
        print("C1 a recu: ", n1)
        sem1.release()


def C2(sema1, sema2, queue):
    while True:
        sem1.acquire()
        n2 = queue.get()
        print("C2 a recu: ", n2)
        sem2.release()


sem1 = mp.Semaphore(0)
sem2 = mp.Semaphore(1)
queue0 = mp.Queue()
queue1 = mp.Queue()
p1 = mp.Process(target=P1, args=(queue0,))
p2 = mp.Process(target=P2, args=(queue1,))
c1 = mp.Process(target=C1, args=(sem1, sem2, queue0))
c2 = mp.Process(target=C2, args=(sem1, sem2, queue1))
p1.start()
p2.start()
c1.start()
c2.start()
p1.join()
p2.join()
c1.join()
c2.join()
