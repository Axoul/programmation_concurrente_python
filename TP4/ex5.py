# Groupe C
# Mallol Fabien
# Chabot Axel

import multiprocessing as mp
import time

def P1(sem21, sem31, sem13, sem12):
    print('je suis P1')
    sem21.release()
    sem31.release()
    sem13.acquire()
    sem12.acquire()
    rdv1()


def P2(sem32, sem12, sem23, sem21):
    print('je suis P2')
    time.sleep(2)
    print('P2 : jai attendu 2sec')
    sem32.release()
    sem12.release()
    sem23.acquire()
    sem21.acquire()
    rdv2()


def P3(sem23, sem32, sem13, sem31):
    print('je suis P3')
    time.sleep(3)
    print('P3 : jai attendu 3sec')
    sem23.release()
    sem13.release()
    sem32.acquire()
    sem31.acquire()
    rdv3()

def rdv1():
    print('rdv1')
    return

def rdv2():
    print('rdv2')
    return

def rdv3():
    print('rdv3')
    return


sem12 = mp.Semaphore(0)
sem23 = mp.Semaphore(0)
sem31 = mp.Semaphore(0)
sem21 = mp.Semaphore(0)
sem32 = mp.Semaphore(0)
sem13 = mp.Semaphore(0)
p1 = mp.Process(target=P1, args=(sem21, sem31, sem13, sem12,))
p2 = mp.Process(target=P2, args=(sem32, sem12, sem23, sem21,))
p3 = mp.Process(target=P3, args=(sem23, sem32, sem13, sem31,))
p1.start()
p2.start()
p3.start()
p1.join()
p2.join()
p3.join()
