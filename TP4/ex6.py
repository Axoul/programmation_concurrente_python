# Groupe C
# Mallol Fabien
# Chabot Axel

import sys
import multiprocessing as mp

def E(sr, se):
    print("E a envoyer un message")
    se.release()
    se.release()
    sr.acquire()
    sr.acquire()
    print("emeteur debloqué")


def R(sr, se):
    print("R a recu un message")
    sr.release()
    se.acquire()
    print("recepteur debloqué")


se = mp.Semaphore(0)
sr = mp.Semaphore(0)
lp = []
for i in sys.argv[1:]:
    print('i = ', i)

    if i == "E":
        p = mp.Process(target=E, args=(sr, se,))
        lp.append(p)
    else:
        p = mp.Process(target=R, args=(sr, se,))
        lp.append(p)

for p in lp:
    p.start()
for p in lp:
    p.join()
