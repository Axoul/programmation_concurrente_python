import multiprocessing as mp
import time, sys

def multitask_sum(inf, sup, sum, mutex, liste):
    somme_loc = 0
    for i in range(inf, sup):
        somme_loc += liste[i]

    with mutex:
        sum.value += somme_loc

n = 1000000
liste = mp.Array('i', n)

for i in range(n):
    liste[i] = 1

debut_normal = time.time()
somme = 0
for i in range(n):
    somme += liste[i]

tps_normal = time.time() - debut_normal

somme_partagee = mp.Value('i', 0)

print("Somme = %d en %f sec" % (somme, tps_normal))

mutex = mp.Lock()
args = int(sys.argv[1])
proc = []
for i in range(args):
    p = mp.Process(target = multitask_sum, args =(int(n/args)*i, (int(n/args)*i)+int(n/args), somme_partagee, mutex, liste))
    proc.append(p)

debut_mp = time.time()
for p in proc:
    p.start()

for p in proc:
    p.join()

tps_mp = time.time() - debut_mp
print("MP:Somme = %d en %f sec" % (somme_partagee.value, tps_mp))
