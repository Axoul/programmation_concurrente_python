import random
import time
import sys
import multiprocessing as mp

def frequence_de_hits_pour_n_essais(nb_iteration):
    count = 0
    for i in range(nb_iteration):
        x = random.random()
        y = random.random()
        if x *x + y *y <= 1:
             count += 1
    return count

def frequence_de_hits_pour_n_essais_mp(nb_iteration, sem, count):
    count_loc = 0
    for i in range(nb_iteration):
        x = random.random()
        y = random.random()
        if x *x + y *y <= 1:
            count_loc += 1

    with sem:
        count.value += count_loc

nb_total_iteration = 10000000
debut_mono = time.time()
nb_hits = frequence_de_hits_pour_n_essais(nb_total_iteration)
tps_mono = time.time() - debut_mono
print("Valeur estimée Pi par la méthode Mono−Processus : %f" % (4 * nb_hits / nb_total_iteration))
print("Temps d'execution Mono-Processus : %f secondes" % (tps_mono))
semaphore = mp.Semaphore()
count_mp = mp.Value('i', 0)
nb_proc = int(sys.argv[1])
proc = []
for i in range(nb_proc):
    p = mp.Process(target=frequence_de_hits_pour_n_essais_mp, args=(int(nb_total_iteration/nb_proc), semaphore, count_mp))
    proc.append(p)

debut_mp = time.time()
for p in proc:
    p.start()

for p in proc:
    p.join()

tps_mp = time.time() - debut_mp
print("Valeur estimée Pi par la méthode avec %d processus : %f" % (nb_proc, 4 * count_mp.value / nb_total_iteration))
print("Temps d'execution avec %d processus : %f secondes" % (nb_proc, tps_mp))
