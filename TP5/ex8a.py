# Mallol Fabien et Chabot Axel
import multiprocessing as mp
import time
import sys

def arc_fct(n, inf, sup, mutex, sum):
    somme_loc = 0
    for i in range(inf, sup):
        somme_loc += 4/(1+(i/n)*(i/n))

    with mutex:
        sum.value += somme_loc

n = 1000000
mutex = mp.Lock()
somme = mp.Value('f', 0)
args = int(sys.argv[1])
proc = []
for i in range(args):
    p = mp.Process(target=arc_fct, args=(n, int(n/args)*i, (int(n/args)*i)+int(n/args), mutex, somme))
    proc.append(p)

debut = time.time()
for p in proc:
    p.start()

for p in proc:
    p.join()

tps = time.time() - debut
print("En %d processus, le calcul a été fait en %f sec. Le resultat : %f" % (args, tps, somme.value/n))
