import multiprocessing as mp
from math import ceil, sqrt
import sys
import time

def nb_premier_mono(nb, liste):
    for i in range(2,nb+1):
        premier = True
        for j in range(2,ceil(sqrt(nb+1))):
            if not i%j:
                premier = False
                #break

        if premier:
            liste.append(i)

N = 10000
liste = []
debut_mono = time.time()
nb_premier_mono(N, liste)
tps_mono = time.time() - debut_mono
print(liste)
print("Calculé en %f" % (tps_mono))
